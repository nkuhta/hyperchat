var Engine = function (io) {
    this.io = io;
    var sockets = {};
    var users = {};
    var nicknames = [];
    var messages = [];

    // counter for unique ids
    var counter = 1;

    /**
     * start socket.io
     */
    io.on('connection', function(socket){
        var userId = counter++;
        console.log("Connected user " + userId);

        /**
         * function to log out user
         */
        var logoutUser = function(){
            console.log('disonnect ' + userId);
            if (users[userId]) {
                var nickname = users[userId];
                console.log('disconnected nickname ' + nickname);
                io.emit('user logged out', nickname);
                nicknames.splice(nicknames.indexOf(nickname), 1);
            }
            delete(sockets[userId]);
            delete(users[userId]);
        };

        // User disconnected
        socket.on('disconnect', function(){
            logoutUser();
        });

        // someone make logout action
        socket.on('logout', function(){
            logoutUser();
        });

        // Users want to login
        socket.on('login', function(nickname){
            if (!nickname || nickname == '__system') {
                socket.emit('exception', "Nickname is empty!");
            } else if (nicknames.indexOf(nickname) != -1) {
                socket.emit('exception', 'Никнейм ' + nickname + ' уже занят! Выберите другое имя.');
            } else {
                socket.emit('you logged in', userId, nickname);
                io.emit('user logged in', nickname);
                users[userId] = nickname;
                nicknames.push(nickname);
            }
        });

        // User send chat message
        socket.on('chat message', function(message){
            var myNickname = users[userId];
            if (!myNickname) {
                socket.emit('exception', 'Not logged in yet!');
            } else {
                var message = {
                    message: message,
                    nickname: myNickname
                };
                io.emit('chat message', message);

                // need to save message on backend
                messages.push(message);
                if (messages.length > 10) { // don't store more that 10 messages
                    messages = messages.splice(1);
                }
            }

        });

        // User want to get users
        socket.on('get users list', function(){
            socket.emit('user list', nicknames);
        });

        // User want to get last messages from backend
        socket.on('get messages', function(){
            console.log(messages);
            socket.emit('old messages', messages);
        });

    });
};



// make engine accessible via "engine()" from server.js
module.exports = function (io) {
    return new Engine(io);
}