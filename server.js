var express = require('express');
var path = require('path');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var engine = require('./engine');

app.use(express.static('public'))

app.get('/',function(req,res){
    res.sendFile(path.resolve('./public/index.html'));
});

// start chat engine to serve socket requests
engine(io);

http.listen(3000, function(){
    console.log('Server started');
});