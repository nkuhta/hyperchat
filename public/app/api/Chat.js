Ext.define("HyperChat.api.Chat", {
    mixins: ['Ext.util.Observable'],
    socket: null,

    // events, that nee to be provided to api listeners
    subscribe: [
        'you logged in',
        'user logged in',
        'user logged out',
        'exception',
        'chat message',
        'user list',
        'old messages',

    ],

    constructor: function () {
        this.mixins.observable.constructor.call(this);
        this.socket = HyperChat.getApplication().getSocket();
        this.createListeners();
    },

    /**
     * Bind listeners and fire events
     */
    createListeners: function () {
        var me = this;
        var create  = function(self, e) {
            self.socket.on(e, function(){
                me.fireEventArgs(e, arguments);
            });
        };
        for(var i = 0; i < this.subscribe.length; i++) {
            var e = this.subscribe[i];
            create(this, e);
        }
    },

    /**
     * Push message to server
     * @param msg
     */
    pushMessage: function (msg) {
        this.socket.emit('chat message', msg);
    },

    login: function (nickname) {
        this.socket.emit('login', nickname);
    },

    logout: function () {
        this.socket.emit('logout');
    },

    /**
     * Get users list from server
     */
    getUsersList: function () {
        this.socket.emit('get users list');
    },

    /**
     * Get saved messages
     */
    getOldMessages: function () {
        this.socket.emit('get messages');
    }

});