/**
 * The main application class. An instance of this class is created by app.js when it
 * calls Ext.application(). This is the ideal place to handle application launch and
 * initialization details.
 */
(function () {
    var application = Ext.define('HyperChat.Application', {
        extend: 'Ext.app.Application',
        requires: ['HyperChat.api.Chat'],

        name: 'HyperChat',

        stores: [
            'Messages',
            'Users'
        ],

        socket: null,
        apis: {},

        launch: function () {
            var nickname = Ext.util.Cookies.get('nickname');
            if (nickname) {
                var chatApi = this.getChatApi();
                chatApi.login(nickname);
            }
        },

        /**
         * Only one socket can be in application.
         */
        getSocket: function () {
            if (!this.socket) {
                this.socket = io();
            }
            return this.socket;
        },

        /**
         * Get api by name
         * @param name
         * @returns {*}
         */
        getApi: function(name) {
            var className = 'HyperChat.api.' + Ext.String.capitalize(name);
            if (!this.apis[className]) {
                this.apis[className] = Ext.create(className);
            }
            return this.apis[className];
        },

        getChatApi: function () {
            return this.getApi('chat');
        },

        onAppUpdate: function () {
            Ext.Msg.confirm('Application Update', 'This application has an update, reload?',
                function (choice) {
                    if (choice === 'yes') {
                        window.location.reload();
                    }
                }
            );
        },


    });
})();