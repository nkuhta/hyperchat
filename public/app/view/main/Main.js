Ext.define("HyperChat.view.main.Main",{
    extend: 'Ext.Panel',
    requires: [
        'HyperChat.view.chat.Chat'
    ],
    layout: {
        type: 'hbox',
        pack: 'center',
        align: 'center'
    },
    items: [{
        xtype: 'chat'
    }]
});