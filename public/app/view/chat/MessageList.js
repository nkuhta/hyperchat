Ext.define('HyperChat.view.chat.MessageList', {
    extend: 'Ext.view.View',
    xtype: 'chat.list',
    requires: ['HyperChat.store.Messages'],
    flex: 1,
    scrollable: true,
    style: {
        borderBottom: '1px solid #5fa2dd'
    },
    tpl: new Ext.XTemplate(
        '<tpl for=".">',
        '<tpl if="nickname != \'__system\'">',
            '<div class="message-block">',
                '<div class="chat-message-body <tpl if="iAmAuthor">chat-message-my</tpl>">',
                    '<p class="nickname">{nickname}</p>',
                    '<p class="message">{message}</p>',
                '</div>',
                '<div class="clear"></div>',
            '</div>',
        '<tpl else>',
            '<div class="system-message">{message}</div>',
        '</tpl>',
        '</tpl>'
    ),
    itemSelector: 'p.chat-message',
    initComponent: function () {
        var me = this;
        this.store = HyperChat.getApplication().getMessagesStore();
        this.callParent(arguments);
        this.store.on('add',function(){
            me.scrollBy(0, 1000, true); // scroll down
        });
    }

});