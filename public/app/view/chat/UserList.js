Ext.define('HyperChat.view.chat.UserList', {
    extend: 'Ext.grid.Panel',
    xtype: 'chat.users',
    requires: ['HyperChat.store.Users'],
    columns: [{
        header: 'Участники',
        dataIndex: 'nickname',
        flex: 1
    }],
    style: {
        borderRight: '1px solid #5fa2dd'
    },
    bbar: [{
        xtype: 'button',
        text: 'Выйти',
        handler: 'onLogoutClick',
        width: '100%',
        style: {
            margin: '3px 8px 4px 0px'
        }
    }],
    initComponent: function () {
        this.store = HyperChat.getApplication().getUsersStore();
        this.callParent(arguments);
        this.getDockedItems()[1].hide();
    },
    setLoggedIn: function () {
        this.getDockedItems()[1].show();
    },
    setLoggedOut: function () {
        this.getDockedItems()[1].hide();
    }
});