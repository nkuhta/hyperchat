Ext.define('HyperChat.view.chat.ChatController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.chat',

    chatStore: null,
    chatApi: null,

    // User's nickname stores in cookies
    nickname: Ext.util.Cookies.get('nickname'),

    init: function () {

        // get dependencies
        this.chatStore = HyperChat.getApplication().getStore('Messages');
        this.usersStore = HyperChat.getApplication().getStore('Users');
        this.chatApi = HyperChat.getApplication().getChatApi();

        // bind all listeners to socket events via api
        this.bindListeners();

        // initial loading
        this.chatApi.getUsersList();
        this.chatApi.getOldMessages();
    },

    /**
     * When user press enter on message field
     * @param field
     * @param e
     * @returns {boolean}
     */
    onMessageFieldSpecialkey: function (field, e) {
        if (e.shiftKey || e.getKey() != e.ENTER) {
            return;
        }
        e.preventDefault();
        if (e.getKey() != e.ENTER || !field.up('form').isValid()) {
            return;
        }
        var value = field.getValue();
        if (Ext.String.trim(value) == "") {
            return false;
        }
        value = value.replace(/\n/g, "<br/>");
        HyperChat.getApplication().getChatApi().pushMessage(value);

        field.setValue('');
        e.preventDefault();

    },

    onLoginSubmit: function (self) {
        var form = self.up('form');
        if (form.isValid()) {
            var data = form.getValues();
            this.chatApi.login(data.nickname);
        }
    },

    /**
     * When user hit enter when he enter login
     * @param field
     * @param e
     */
    onNicknameFieldSpecialkey: function(field, e) {
        var ctrl = this;
        if (e.getKey() == e.ENTER) {
            ctrl.onLoginSubmit(field);
        }
    },

    onLogoutClick: function(btn) {
        this.chatApi.logout();
        this.view.down('form').setLoggedOut();
        this.view.down('grid').setLoggedOut();
        Ext.util.Cookies.set('nickname', '');
    },

    /**
     * Bind listeners to socket events via api
     */
    bindListeners: function () {
        var ctrl = this;
        var view = this.view;

        /**
         * When message come from socket
         */
        this.chatApi.on('chat message', function(message){
            ctrl.chatStore.addMessage({
                message: message.message,
                nickname: message.nickname,
                iAmAuthor: message.nickname == ctrl.nickname
            });
        });

        /**
         * When user with THIS socket was logged in
         */
        this.chatApi.on('you logged in', function(userId, nickname){
            view.down('form').setLoggedIn();
            view.down('grid').setLoggedIn();
            ctrl.nickname = nickname;
            Ext.util.Cookies.set('nickname', nickname);
        });

        /**
         * Any user was logged in
         */
        this.chatApi.on('user logged in', function(nick){
            ctrl.chatStore.addMessage({
                nickname: '__system',
                message: nick + " вошел в чат"
            });
            ctrl.usersStore.addUser(nick);
        });

        /**
         * Any user logged out
         */
        this.chatApi.on('user logged out', function(nick){
            ctrl.chatStore.addMessage({
                nickname: '__system',
                message: nick + " вышел из чата"
            });
            ctrl.usersStore.removeUser(nick);
        });

        /**
         * Some exception was on backend
         */
        this.chatApi.on('exception', function(message){
            alert(message);
        });

        /**
         * User list came from socket
         */
        this.chatApi.on('user list', function(nicknames){
            ctrl.usersStore.loadUsers(nicknames);
        });

        /**
         * Last 10-messages came from socket
         */
        this.chatApi.on('old messages', function(messages){
            Ext.each(messages, function(message){
                if (message.nickname == ctrl.nickname) {
                    message.iAmAuthor = true;
                }
            });
            ctrl.chatStore.loadMessages(messages);
        });
    }
});
