/**
 * Base view for chat
 */
Ext.define("HyperChat.view.chat.Chat",{
    extend: 'Ext.Panel',
    controller: 'chat',
    requires: [
        'HyperChat.view.chat.MessageList',
        'HyperChat.view.chat.UserList',
        'HyperChat.view.chat.Form',

        'HyperChat.view.chat.ChatController'
    ],
    layout: {
        type: 'hbox',
        pack: 'start',
        align: 'stretch'
    },
    title: 'Hyperchat',
    xtype: 'chat',
    width: 600,
    height: 600,
    border: true,
    items: [{
        xtype: 'chat.users',
        flex: 1,
    },{
        xtype: 'panel',
        layout: {
            type: 'vbox',
            pack: 'start',
            align: 'stretch'
        },
        flex: 2,
        items: [{
            xtype: 'chat.list'
        },{
            xtype: 'chat.form'
        }]
    }]
});