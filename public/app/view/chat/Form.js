Ext.define("HyperChat.view.chat.Form",{
    extend: 'Ext.form.Panel',
    xtype: 'chat.form',
    style: {
        margin: '10px 10px 0 10px'
    },
    initComponent: function () {
        this.items = [
            this.getNotLoggedInContainer(),
            this.getLoggedInContainer()
        ];
        this.callParent(arguments);
    },

    /**
     * There form controls can visible when not logged in
     * @returns {Ext.form.FieldContainer|*}
     */
    getNotLoggedInContainer: function () {
        if (!this.notLoggedInContainer ) {
            this.notLoggedInContainer = Ext.create("Ext.form.FieldContainer", {
                xtype: 'fieldcontainer',
                layout: 'hbox',
                items: [{
                    xtype: 'textfield',
                    emptyText: 'Ваше имя',
                    fieldLabel: 'Войдите',
                    labelWidth: 58,
                    name: 'nickname',
                    flex: 3,
                    allowBlank: false,
                    style: {
                        marginRight: '10px'
                    },
                    listeners: {
                        specialkey: 'onNicknameFieldSpecialkey'
                    }
                }, {
                    xtype: 'button',
                    text: 'Войти',
                    flex: 1,
                    handler: 'onLoginSubmit'
                }]
            });
        }
        return this.notLoggedInContainer;
    },
    /**
     * these form controls will see only logged in users
     */
    getLoggedInContainer: function () {
        if (!this.loggedInContainer ) {
            this.loggedInContainer = Ext.create("Ext.form.FieldContainer", {
                xtype: 'fieldcontainer',
                hidden: true,
                disabled: true,
                layout: 'hbox',
                items: [{
                    xtype: 'textarea',
                    grow: true,
                    flex: 3,
                    growMin: 30,
                    enterIsSpecial: true,
                    growMax: 120,
                    growAppend: 30,
                    emptyText: 'Введите сообщение...',
                    listeners: {
                        specialkey: 'onMessageFieldSpecialkey'
                    }
                }]
            });
        }
        return this.loggedInContainer;
    },
    setLoggedIn: function () {
        this.notLoggedInContainer.hide();
        this.notLoggedInContainer.disable();
        this.loggedInContainer.enable();
        this.loggedInContainer.show();
    },

    setLoggedOut: function () {
        this.notLoggedInContainer.show();
        this.notLoggedInContainer.enable();
        this.loggedInContainer.disable();
        this.loggedInContainer.hide();
    }
});