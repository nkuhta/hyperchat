/**
 * Store keep users
 */
Ext.define('HyperChat.store.Users', {
    extend: 'Ext.data.Store',
    alias: 'store.users',

    fields: [
        'nickname',
        'you'
    ],

    proxy: 'memory',

    addUser: function (nick) {
        this.add({
            nickname: nick
        });
    },

    removeUser: function (nick) {
        var user = this.findRecord('nickname', nick);
        if (user) {
            this.remove(user);
        }
    },

    loadUsers: function(nicknames) {
        this.removeAll();
        for(var i = 0; i < nicknames.length; i++) {
            this.addUser(nicknames[i]);
        }
    }


});