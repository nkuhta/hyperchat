/**
 * Store for messages
 */
Ext.define('HyperChat.store.Messages',{
    extend: 'Ext.data.Store',
    alias: 'store.messages',

    fields: [
        'nickname',
        'text',
        'iAmAuthor'
    ],

    proxy: 'memory',

    addMessage: function (message) {
        message = Ext.clone(message); // don't modify source message
        this.add({
            message: message.message,
            nickname: message.nickname,
            iAmAuthor: message.iAmAuthor
        })
    },

    /**
     * Massive add messages to store
     * @param msgs
     */
    loadMessages: function (msgs) {
        this.add(msgs);
    }
});